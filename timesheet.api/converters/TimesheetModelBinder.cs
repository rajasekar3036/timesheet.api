﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace timesheet.api.converters
{
    public class TimesheetModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            var modelName = bindingContext.ModelName;

            // Try to fetch the value of the argument by name
            var valueProviderResult =
                bindingContext.ValueProvider.GetValue(modelName);

            HttpRequest req = bindingContext.HttpContext.Request;
            // Allows using several time the stream in ASP.Net Core
            //req.EnableRewind();

            string serialized;

            // Arguments: Stream, Encoding, detect encoding, buffer size 
            // AND, the most important: keep stream opened
            using (StreamReader reader = new StreamReader(req.Body, Encoding.UTF8, true, 1024, true))
            {
                serialized = reader.ReadToEnd();
            } 

            if (string.IsNullOrWhiteSpace(serialized) )
            {
                return Task.CompletedTask;
            }

            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            //string serialized = JsonConvert.SerializeObject(item, jsonSerializerSettings);
            List<timesheet.model.Timesheet > deserialized = JsonConvert.DeserializeObject<List<timesheet.model.Timesheet>>(serialized, jsonSerializerSettings);
            
            bindingContext.Result = ModelBindingResult.Success(deserialized);
            return Task.CompletedTask;
        }
    }
}
