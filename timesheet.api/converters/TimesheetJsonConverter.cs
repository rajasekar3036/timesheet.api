﻿using Newtonsoft.Json.Linq;
using System;
using timesheet.model;

namespace timesheet.api.converters
{
    public class TimesheetJsonConverter : JsonCreationConverter<Timesheet>
    {
        protected override Timesheet Create(Type objectType, JObject jObject)
        {
            if (jObject == null) throw new ArgumentNullException("jObject");
            return new Timesheet();

        }
    }
}
