﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using timesheet.api.converters;

using timesheet.business;
using timesheet.business.views;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/timesheet")]
    [ApiController]
    public class TimesheetController : ControllerBase
    {
        private readonly ITimesheetService timesheetService;
        public TimesheetController(ITimesheetService timesheetService)
        {
            this.timesheetService = timesheetService;
        }

        [HttpGet("getall")]
        public IActionResult GetAll(string text)
        {
            var items = this.timesheetService.GetTimesheets();
            return new ObjectResult(items);
        }

        [HttpGet("get-employee")]
        public IActionResult GetByEmployee(int id)
        {
            var items = this.timesheetService.GetByEmployee(id);
            return new ObjectResult(items);
        }

        [HttpGet("get-week")]
        public IActionResult GetByWeek(int id, DateTime currentDate)
        { 
            TimesheetView data = new TimesheetView();
            data.LoadData(this.timesheetService, id, currentDate);
            return new ObjectResult(data);
        }

        [HttpPost("save")]
        public IActionResult SaveTimesheet([FromBody] [ModelBinder(typeof(TimesheetModelBinder))]List<Timesheet> timesheet)
        {
            var items = this.timesheetService.SaveTimesheet(timesheet);
            return new ObjectResult(items);
        } 

        [HttpPost("save-single")]
        public IActionResult SaveTimesheetSingle(Timesheet timesheet)
        {
            var items = this.timesheetService.SaveTimesheet( new List<Timesheet>() { timesheet });
            return new ObjectResult(items);
        }
    }

     
}