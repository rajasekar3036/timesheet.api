﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace timesheet.model
{
    public class Employee
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [StringLength(10)]
        [Required]
        public string Code { get; set; }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        //stores the total hours wored 
        public float TotalHoursWorked { get; set; }

        //stores the average hours wored per week
        public float AverageHoursWorked { get; set; }


        public ICollection<Timesheet> Timesheets { get; set; }
    }
}
