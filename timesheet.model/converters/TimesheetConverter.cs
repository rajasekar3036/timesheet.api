﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using timesheet.model;

namespace timesheet.model.converters
{
    public class TimesheetConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context,CultureInfo culture, object value)
        {
            if (value is string)
            {
                Timesheet timesheet;
                if (Timesheet.TryParse((string)value, out timesheet))
                {
                    return timesheet;
                }
            }
            return base.ConvertFrom(context, culture, value);
        }
    }
}
