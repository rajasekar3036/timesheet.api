﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using timesheet.model.converters;

namespace timesheet.model
{
    //[TypeConverter(typeof(TimesheetConverter))]
    public class Timesheet
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [Required]
        public int EmployeeId { get; set; } 
        
        public Employee Employee { get; set; }

        [Required]
        public int TaskId { get; set; } 
       
        public Task Task { get; set; }

        [Required]
        public DateTime TaskDate { get; set; }

        [Required]
        public float HoursWorked { get; set; }


        public static bool TryParse(string s, out Timesheet result)
        {
            result = null;

            var parts = s.Split(',');
            if (parts.Length != 2)
            {
                return false;
            }

            double latitude, longitude;
            if (double.TryParse(parts[0], out latitude) &&
                double.TryParse(parts[1], out longitude))
            {
                result = new Timesheet() {  };
                return true;
            }
            return false;
        }

    }
}
