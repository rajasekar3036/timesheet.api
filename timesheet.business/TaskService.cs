﻿using System;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class TaskService : ITaskService
    {
        public TimesheetDb db { get; }
        public TaskService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<Task> GetTasks()
        {
            return this.db.Tasks;
        }

        private bool _disposed;

        //created a wrapper for dispose for already disposed objects
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    this.db.Dispose();
                }
            }
            _disposed = true;
        }

        //implemented the dispose to safely dispose the database
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    public interface ITaskService : IDisposable
    {
        IQueryable<Task> GetTasks();
    }
}
