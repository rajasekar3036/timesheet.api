using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class TimesheetService : ITimesheetService
    {
        public TimesheetDb db { get; }
        public TimesheetService(TimesheetDb dbContext)
        {
            db = dbContext;
        }

        public IQueryable<Timesheet> GetByEmployee(int id)
        {
            return db.Timesheets.Where(i => i.EmployeeId == id);
        }

        public IQueryable<Timesheet> GetByWeek(int id, DateTime weekStart, DateTime weekEnd)
        {
            return db.Timesheets.Include(s => s.Employee).Include(s => s.Task).Where(i => i.EmployeeId == id && i.TaskDate >= weekStart && i.TaskDate <= weekEnd);
        }

        public IQueryable<Timesheet> GetTimesheets()
        {
            return db.Timesheets;
        }

        public int SaveTimesheet(ICollection<Timesheet> timesheet)
        {
            if (timesheet != null)
            {
                try
                {
                    int id = -1;
                    foreach (var item in timesheet)
                    {
                        if (item.Id > 0)
                            db.Timesheets.Update(item);
                        else
                        {
                            if (item.HoursWorked > 0)
                            {
                                var data = db.Timesheets.Where(i => i.TaskId == item.TaskId && i.EmployeeId == item.EmployeeId && i.TaskDate == item.TaskDate).FirstOrDefault();
                                if (data != null && data.Id > 0)
                                {
                                    data.HoursWorked = item.HoursWorked;
                                }
                                else
                                {                                    
                                    item.Id = 0;
                                    db.Timesheets.Add(item);
                                }
                                    
                            }
                        }
                    }
                    int result = db.SaveChanges();

                    var empIds = timesheet.GroupBy(i => i.EmployeeId).Select(i => i.Key).ToList();
                    foreach (var EmpId in empIds)
                    {
                        float TotalHoursWorked = db.Timesheets.Where(i => i.EmployeeId == EmpId).Sum(i => i.HoursWorked);
                        DateTime minDate = db.Timesheets.Where(i => i.EmployeeId == EmpId).Min(i => i.TaskDate);
                        DateTime maxDate = db.Timesheets.Where(i => i.EmployeeId == EmpId).Max(i => i.TaskDate);
                        double weeks = Math.Ceiling((maxDate - minDate).TotalDays / 7);
                        float AverageHoursWorked = (float)(TotalHoursWorked / weeks);

                        Employee emp = db.Employees.Where(i => i.Id == EmpId).Single();
                        emp.TotalHoursWorked = TotalHoursWorked;
                        emp.AverageHoursWorked = AverageHoursWorked;
                        db.SaveChanges();
                    }

                    return result;

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return -1;
                }

            }
            else
                return -1;
        }

        private bool _disposed;

        //created a wrapper for dispose for already disposed objects
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            _disposed = true;
        }

        //implemented the dispose to safely dispose the database
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    public interface ITimesheetService : IDisposable
    {
        IQueryable<Timesheet> GetByEmployee(int id);
        IQueryable<Timesheet> GetByWeek(int id, DateTime weekStart, DateTime weekEnd);
        IQueryable<Timesheet> GetTimesheets();
        int SaveTimesheet(ICollection<Timesheet> timesheet);
    }
}
