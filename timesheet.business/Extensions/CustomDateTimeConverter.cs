﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.business.Extensions
{
   public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter()
        {
            base.DateTimeFormat = "dd-MMM-yyyy";
        }

        public CustomDateTimeConverter(string format)
        {
            base.DateTimeFormat = format;
        }
    }
}
