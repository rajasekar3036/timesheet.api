using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.business.views
{ 

    public partial class TaskList
    { 
        [JsonProperty("taskId", NullValueHandling = NullValueHandling.Ignore)]
        public long? TaskId { get; set; }

        [JsonProperty("taskName", NullValueHandling = NullValueHandling.Ignore)]
        public string TaskName { get; set; }

        [JsonProperty("timing", NullValueHandling = NullValueHandling.Ignore)]
        public List<Summary> Timing { get; set; }

    [JsonProperty("sunday", NullValueHandling = NullValueHandling.Ignore)]
    public Summary Sunday { get; set; }
    [JsonProperty("monday", NullValueHandling = NullValueHandling.Ignore)]
    public Summary Monday { get; set; }
    [JsonProperty("tuesday", NullValueHandling = NullValueHandling.Ignore)]
    public Summary Tuesday { get; set; }
    [JsonProperty("wednesday", NullValueHandling = NullValueHandling.Ignore)]
    public Summary Wednesday { get; set; }
    [JsonProperty("thursday", NullValueHandling = NullValueHandling.Ignore)]
    public Summary Thursday { get; set; }
    [JsonProperty("friday", NullValueHandling = NullValueHandling.Ignore)]
    public Summary Friday { get; set; }
    [JsonProperty("saturday", NullValueHandling = NullValueHandling.Ignore)]
    public Summary Saturday { get; set; }

  }
}
