using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using timesheet.business.Extensions;

namespace timesheet.business.views
{

  public partial class TimesheetView
  {
    [JsonProperty("week", NullValueHandling = NullValueHandling.Ignore)]
    public Week Week { get; set; }

    [JsonProperty("taskList", NullValueHandling = NullValueHandling.Ignore)]
    public List<TaskList> TaskList { get; set; }

    [JsonProperty("summary", NullValueHandling = NullValueHandling.Ignore)]
    public TaskList Summary { get; set; }


    public static TimesheetView FromJson(string json) => JsonConvert.DeserializeObject<TimesheetView>(json, timesheet.business.views.Converter.Settings);


    public void LoadData(ITimesheetService timesheetService, int id, DateTime currentDate)
    {
      DateTime weekStart = currentDate.StartOfWeek(DayOfWeek.Sunday);
      DateTime weekEnd = currentDate.EndOfWeek(DayOfWeek.Sunday);
      var items = timesheetService.GetByWeek(id, weekStart, weekEnd);
      if (items != null)
      {
        List<model.Timesheet> data = items.ToList();
        Week = new Week() { CurrentWeekStart = weekStart, CurrentWeekEnd = weekEnd, Current = currentDate, Next = weekEnd.AddDays(1), Prev = weekStart.AddDays(-7) };
        TaskList = new List<TaskList>();

        Summary sunday = null;
        Summary monday = null;
        Summary tuesday = null;
        Summary wednesday = null;
        Summary thursday = null;
        Summary friday = null;
        Summary saturday = null;

        var taskIds = items.GroupBy(i => i.TaskId).Select(i => i.Key);
        foreach (var item in taskIds)
        {
          model.Timesheet task = items.Where(i => i.TaskId == item).FirstOrDefault();
          TaskList taskData = new TaskList();
          if (task != null && task.Id > 0)
          {
            taskData.TaskId = task.TaskId;
            if (task.Task != null)
              taskData.TaskName = task.Task.Name;
            taskData.Timing = new List<Summary>();

            model.Timesheet day1 = items.Where(i => i.TaskId == item && i.TaskDate == weekStart).FirstOrDefault();
            model.Timesheet day2 = items.Where(i => i.TaskId == item && i.TaskDate == weekStart.AddDays(1)).FirstOrDefault();
            model.Timesheet day3 = items.Where(i => i.TaskId == item && i.TaskDate == weekStart.AddDays(2)).FirstOrDefault();
            model.Timesheet day4 = items.Where(i => i.TaskId == item && i.TaskDate == weekStart.AddDays(3)).FirstOrDefault();
            model.Timesheet day5 = items.Where(i => i.TaskId == item && i.TaskDate == weekStart.AddDays(4)).FirstOrDefault();
            model.Timesheet day6 = items.Where(i => i.TaskId == item && i.TaskDate == weekStart.AddDays(5)).FirstOrDefault();
            model.Timesheet day7 = items.Where(i => i.TaskId == item && i.TaskDate == weekStart.AddDays(6)).FirstOrDefault();



            if (sunday == null) sunday = new Summary(day1, weekStart, taskData.TaskId); else if (day1 != null) sunday.HoursWorked += day1.HoursWorked;
            if (monday == null) monday = new Summary(day2, weekStart.AddDays(1), taskData.TaskId); else if (day2 != null) monday.HoursWorked += day2.HoursWorked;
            if (tuesday == null) tuesday = new Summary(day3, weekStart.AddDays(2), taskData.TaskId); else if (day3 != null) tuesday.HoursWorked += day3.HoursWorked;
            if (wednesday == null) wednesday = new Summary(day4, weekStart.AddDays(3), taskData.TaskId); else if (day4 != null) wednesday.HoursWorked += day4.HoursWorked;
            if (thursday == null) thursday = new Summary(day5, weekStart.AddDays(4), taskData.TaskId); else if (day5 != null) thursday.HoursWorked += day5.HoursWorked;
            if (friday == null) friday = new Summary(day6, weekStart.AddDays(5), taskData.TaskId); else if (day6 != null) friday.HoursWorked += day6.HoursWorked;
            if (saturday == null) saturday = new Summary(day7, weekStart.AddDays(6), taskData.TaskId); else if (day7 != null) saturday.HoursWorked += day7.HoursWorked;

            Summary sumDay1 = new Summary(day1, weekStart, taskData.TaskId);
            Summary sumDay2 = new Summary(day2, weekStart.AddDays(1), taskData.TaskId);
            Summary sumDay3 = new Summary(day3, weekStart.AddDays(2), taskData.TaskId);
            Summary sumDay4 = new Summary(day4, weekStart.AddDays(3), taskData.TaskId);
            Summary sumDay5 = new Summary(day5, weekStart.AddDays(4), taskData.TaskId);
            Summary sumDay6 = new Summary(day6, weekStart.AddDays(5), taskData.TaskId);
            Summary sumDay7 = new Summary(day7, weekStart.AddDays(6), taskData.TaskId);
            taskData.Sunday = sumDay1;
            taskData.Monday = sumDay2;
            taskData.Tuesday = sumDay3;
            taskData.Wednesday = sumDay4;
            taskData.Thursday = sumDay5;
            taskData.Friday = sumDay6;
            taskData.Saturday = sumDay7;

            //taskData.Timing.Add(new Summary(day1, weekStart));
            //taskData.Timing.Add(new Summary(day2, weekStart.AddDays(1)));
            //taskData.Timing.Add(new Summary(day3, weekStart.AddDays(2)));
            //taskData.Timing.Add(new Summary(day4, weekStart.AddDays(3)));
            //taskData.Timing.Add(new Summary(day5, weekStart.AddDays(4)));
            //taskData.Timing.Add(new Summary(day6, weekStart.AddDays(5)));
            //taskData.Timing.Add(new Summary(day7, weekStart.AddDays(6)));
            TaskList.Add(taskData);
          }
        }
        this.Summary = new TaskList();
        this.Summary.Sunday = sunday;
        this.Summary.Monday = monday;
        this.Summary.Tuesday = tuesday;
        this.Summary.Wednesday = wednesday;
        this.Summary.Thursday = thursday;
        this.Summary.Friday = friday;
        this.Summary.Saturday = saturday;
        /*
        Summary = new List<Summary>();
        Summary.Add(sunday);
        Summary.Add(monday);
        Summary.Add(tuesday);
        Summary.Add(wednesday);
        Summary.Add(thursday);
        Summary.Add(friday);
        Summary.Add(saturday);
        */

      }
    }
  }

  public static class Serialize
  {
    public static string ToJson(this TimesheetView self) => JsonConvert.SerializeObject(self, timesheet.business.views.Converter.Settings);
  }

  internal static class Converter
  {
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
      MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
      DateParseHandling = DateParseHandling.None,
      Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
    };
  }


}
