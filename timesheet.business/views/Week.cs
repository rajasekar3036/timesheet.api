﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using timesheet.business.Extensions;

namespace timesheet.business.views
{

    public partial class Week
    {
        [JsonConverter(typeof(CustomDateTimeConverter), "dd-MMM-yyyy")]
        [JsonProperty("prev", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Prev { get; set; }

        [JsonConverter(typeof(CustomDateTimeConverter), "dd-MMM-yyyy")]
        [JsonProperty("current_week_start", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime CurrentWeekStart { get; set; }

        [JsonConverter(typeof(CustomDateTimeConverter), "dd-MMM-yyyy")]
        [JsonProperty("current_week_end", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime CurrentWeekEnd { get; set; }

        [JsonConverter(typeof(CustomDateTimeConverter), "dd-MMM-yyyy")]
        [JsonProperty("current", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Current { get; set; }

        [JsonConverter(typeof(CustomDateTimeConverter), "dd-MMM-yyyy")]
        [JsonProperty("next", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Next { get; set; }
    }

}
