using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using timesheet.business.Extensions;

namespace timesheet.business.views
{
  public partial class Summary
  {
    [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
    public long? Id { get; set; }

    [JsonConverter(typeof(CustomDateTimeConverter), "dd-MMM-yyyy")]
    [JsonProperty("taskDate", NullValueHandling = NullValueHandling.Ignore)]
    public DateTime? TaskDate { get; set; }

    [JsonProperty("hoursWorked", NullValueHandling = NullValueHandling.Ignore)]
    public float? HoursWorked { get; set; }

    [JsonProperty("day", NullValueHandling = NullValueHandling.Ignore)]
    public string Day { get; set; }

    [JsonProperty("taskId", NullValueHandling = NullValueHandling.Ignore)]
    public long? TaskId { get; set; }

    [JsonProperty("employeeId", NullValueHandling = NullValueHandling.Ignore)]
    public long? EmployeeId { get; set; }

    public Summary(model.Timesheet day, DateTime taskDate, long? taskId)
    {
      LoadData(day, taskDate, taskId);
    }

    public void LoadData(model.Timesheet day, DateTime taskDate, long? taskId)
    {
      TaskDate = taskDate;
      TaskId = taskId;
      Day = taskDate.ToString("dddd");
      HoursWorked = 0;
      EmployeeId = 0;
      if (day != null && day.Id > 0)
      {
        Id = day.Id;
        HoursWorked = day.HoursWorked;
      }

    }
  }
}
